Felixent| Our Services

Our Services
============

### What is good business without strategy?

  

IT CONSULTING
-------------

CIO consulting | Project Management | Recruiting experts and you the best IT professionals.

  

IT INFRASTRUCTURE
-----------------

Database development & migration | Disaster recovery & Security planning | Network design & build Virtual server environment.

  

WEB DEVELOPMENT
---------------

Website Audit | Website Design | Web Development (E-Commerce, SEO, Site Map, etc.)

  

MANAGED SERVICES
----------------

24/7/365 Help-desk support & network monitoring | Fully outsourced or “co-managed” Cloud-services | Grow-as-you-go, IT procurement.